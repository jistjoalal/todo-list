Rails.application.routes.draw do

  devise_for :users
  get 'comments/create'

  resources :todo_lists do
    resources :todo_items do
      member do
        patch :toggle
      end
      collection do
        patch :clear_all
        patch :complete_all
        delete :delete_all
      end
    end
  end
  resources :comments
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "todo_lists#index"
end
