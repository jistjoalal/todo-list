class AddDueToTodoItem < ActiveRecord::Migration[5.0]
  def change
    add_column :todo_items, :due, :datetime
  end
end
