class CreateComments < ActiveRecord::Migration[5.0]
  def change
    create_table :comments do |t|
      t.text :body
      t.string :author, default: 'anon'

      t.timestamps
    end
  end
end
