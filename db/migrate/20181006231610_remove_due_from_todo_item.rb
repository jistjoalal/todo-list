class RemoveDueFromTodoItem < ActiveRecord::Migration[5.0]
  def change
    remove_column :todo_items, :due, :datetime
  end
end
