class AddCategoryToTodoItems < ActiveRecord::Migration[5.0]
  def change
    add_column :todo_items, :category, :string
  end
end