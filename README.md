

[Link to original project on Github](https://github.com/SteveChenDC/todo-list)

# Getting Started
```sh
git clone git@gitlab.com:jistjoalal/todo-list.git
cd todo-list
bundle install
rails db:migrate
rails server
```
## Deploy
```sh
heroku create my-heroku-todo-list
git push heroku
heroku run rails db:migrate
heroku open
```