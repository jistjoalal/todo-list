class TodoItemsController < ApplicationController
  before_action :set_todo_list
  before_action :set_todo_item,
                except: [:create, :complete_all, :clear_all, :delete_all]

  def create
    @todo_item = @todo_list.todo_items.create(todo_item_params)
    redirect_to todo_list_path(@todo_list, anchor: 'add')
  end

  def edit 
  end

  def update
    if @todo_item.update_attributes(todo_item_params)
      redirect_to @todo_list
    end
  end

  def destroy
    if @todo_item.destroy
      flash[:success] = "Todo List item was deleted"
    else
      flash[:error] = "Todo List item could not be deleted."
    end
    redirect_to todo_list_path(@todo_list, anchor: 'anchor')
  end

  def toggle
    if @todo_item.completed_at.nil?
      @todo_item.update_attribute(:completed_at, Time.now)
      redirect_to todo_list_path(@todo_list, anchor: @todo_item.id)
    else
      @todo_item.update_attribute(:completed_at, nil)
      redirect_to todo_list_path(@todo_list, anchor: @todo_item.id)
    end
  end

  def complete_all
    @todo_list.todo_items.each do |item|
      item.update_attribute(:completed_at, Time.now)
    end
    redirect_to todo_list_path(@todo_list, anchor: 'add')
  end

  def clear_all
    @todo_list.todo_items.each do |item|
      item.update_attribute(:completed_at, nil)
    end
    redirect_to todo_list_path(@todo_list, anchor: 'add')
  end

  def delete_all
    @todo_list.todo_items.each do |item|
      item.delete
    end
    redirect_to todo_list_path(@todo_list)
  end

  private

  def set_todo_list
    @todo_list = TodoList.find(params[:todo_list_id])
  end

  def set_todo_item
    @todo_item = @todo_list.todo_items.find(params[:id])
  end

  def todo_item_params
    params[:todo_item].permit(:content, :category)
  end
end
