class CommentsController < ApplicationController
  before_action :set_comment, only: [:destroy]

  def create
    @comment = Comment.new(comment_params)
    @comment.save
    redirect_to root_path(anchor: 'comments')
  end

  def destroy
    @comment.destroy
    redirect_to root_path(anchor: 'pagination')
  end

  private

  def set_comment
    @comment = Comment.find(params[:id])
  end

  def comment_params
    params.require(:comment).permit(:body, :author)
  end
end
